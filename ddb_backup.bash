#!/bin/bash -ex

echo "Installing awscli"

pip install awscli

echo "Backing up DynamoDB"

aws dynamodb --table-name $TABLE_NAME --backup-name date

echo "Backup done"