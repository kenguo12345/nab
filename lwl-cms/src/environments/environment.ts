export const environment = {
  envName: "development",
  production: false,
  formsEnabled: true,
  apiUrl: 'https://lwl-dev-ause-api-as45c01943.azurewebsites.net',
  apiKey: 'xyz123',
  aadAuth: {
    enabled: true,
    tenant: 'fgmnt.tech',
    clientId: '9f0b6837-7002-46bf-b9dd-9b5259684dff',
  }
};
