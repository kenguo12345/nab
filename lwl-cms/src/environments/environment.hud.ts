export const environment = {
    envName: "hud",
    production: false,
    formsEnabled: true,
    apiUrl: 'https://lwl-hud-app-ause.azurewebsites.net',
    apiKey: 'xyz123',
    aadAuth: {
      enabled: false,
      tenant: 'fgmnt.tech',
      clientId: '9f0b6837-7002-46bf-b9dd-9b5259684dff',
    }
  };
  