export const environment = {
  envName: "local",
  production: false,
  formsEnabled: true,
  apiUrl: 'http://localhost:51121',
  apiKey: 'xyz123',
  aadAuth: {
    enabled: false,
    tenant: 'fgmnt.tech',
    clientId: '9f0b6837-7002-46bf-b9dd-9b5259684dff',
  }
};
