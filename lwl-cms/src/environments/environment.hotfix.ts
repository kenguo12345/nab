export const environment = {
    envName: "hotfix",
    production: false,
    formsEnabled: true,
    apiUrl: 'https://lwl-htfx-ause-api-rg076011c3.azurewebsites.net',
    apiKey: 'xyz123',
    aadAuth: {
      enabled: false,
      tenant: 'fgmnt.tech',
      clientId: '9f0b6837-7002-46bf-b9dd-9b5259684dff',
    }
  };
  