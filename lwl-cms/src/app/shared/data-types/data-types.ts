export enum VarietalInfoMode {
    World = "WORLD",
    Screen = "SCREEN",
}

export enum TipFactType {
    Point = "POINT",
    Serve = "SERVE",
    Peak = "PEAK",
    Alcohol = "ALCOHOL",
}