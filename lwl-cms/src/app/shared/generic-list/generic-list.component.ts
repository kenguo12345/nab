import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { AddItemModalComponent } from 'src/app/shared/add-item-modal/add-item-modal.component';
import { first } from 'rxjs/operators';
import { LocaleService, Locality } from 'src/app/services/locale.service';
import { isObject, isNumber } from 'util';

@Component({
  templateUrl: './generic-list.component.html',
  styleUrls: ['./generic-list.component.css']
})
export class GenericListComponent implements OnInit {

  formKeys: any;
  sortKey: string;
  filter: string;
  type: string;
  addModal: BsModalRef;
  objectData: Array<any>;
  objectDataByRegion: Object;
  showListByRegionTab: boolean;
  showCustomSortTab: boolean;

  constructor(
    private route: ActivatedRoute,
    private data: DataService,
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private locale: LocaleService) {
    this.type = this.route.snapshot.data["type"];
    this.showListByRegionTab = false;
  }

  ngOnInit() {
    this.data.getList(this.type).subscribe(
      d => {
        this.objectData = d;
        this.sortKey = this.route.snapshot.data["sortKey"];
        if (this.sortKey) this.showCustomSortTab = true;
        if (this.sortKey) {
          this.objectData.sort((a, b) => a[this.sortKey] - b[this.sortKey]);
        }
        this.formKeys = d.map((e: { key: string; }) => e.key);
        this.showListByRegionTab = this.objectData[0].regions;
      });
  }

  regionNameFromKey(key: string): string {
    const locale = this.locale.regions.find(x => x.code === key);
    return locale ? locale.name : 'Locale Not Found';
  }

  get keys(): string[] {
    return this.formKeys ? this.formKeys.sort().filter(e => {
      return this.filter ? e.includes(this.filter) : true;
    }) : [];
  }

  get customSortKeys(): string[] {
    if (!this.objectData) return [];
    return this.objectData.sort((a, b) => this.genericSortFunction(a, b)).map((e: { key: string; }) => e.key).filter(e => {
      return this.filter ? e.includes(this.filter) : true;
    });
  }

  get customSortTitle(): string {
    return "Sort by key: " + this.sortKey;
  }

  edit(key?: string) {
    this.router.navigate([`/${this.type}`, key]);
  }

  getListItemURL(key?: string) {
    return `/${this.type}/${key}/edit`;
  }

  add() {
    this.addModal = this.modalService.show(
      AddItemModalComponent, {
      initialState: { title: `Add new ${this.type}` },
      class: 'modal-m',
      ignoreBackdropClick: true,
      keyboard: false
    });

    this.addModal.content.create.pipe(first()).subscribe((key: string) => {
      const docType = this.type;
      this.data.saveItem(`${docType}:${key}`, {
        key,
        docType
      }).subscribe(d => {
        this.toastr.success(`${this.type} '${key}' created successfully.`);
        this.router.navigate([`/${this.type}`, key, 'edit']);
      }, err => {
        console.log(`error creating ${this.type}`);
      });
    });
  }

  delete(key: string) {
    if (!confirm(`Are you sure you want to delete ${this.type} '${key}'?`)) { return; }
    const id = `${this.type}:${key}`;
    this.data.deleteItem(id).subscribe(d => {
      _.pull(this.formKeys, key);
      this.toastr.success(`${this.type} '${key}' deleted.`);
    });
  }

  genericSortFunction(a: any, b: any) {
    if (!a[this.sortKey] && !b[this.sortKey]) return 0;
    if (a[this.sortKey] && !b[this.sortKey]) return -1;
    if (!a[this.sortKey] && b[this.sortKey]) return 1;
    if (isObject(a[this.sortKey]) && isObject(b[this.sortKey])) {
      if (!isNumber(a[this.sortKey]['-']['-'])) {
        // sort as string
        return a[this.sortKey]['-']['-'].localeCompare(b[this.sortKey]['-']['-']);
      } else {
        // sort as number
        return a[this.sortKey]['-']['-'] - b[this.sortKey]['-']['-'];
      }
    }
    if (isNumber(a[this.sortKey])) return a[this.sortKey] - b[this.sortKey];
    return a[this.sortKey].localeCompare(b[this.sortKey]);
  }

  get dataByRegion(): any {
    if (this.objectDataByRegion) return this.objectDataByRegion;
    if (!this.objectData) return;
    this.objectDataByRegion = {};
    this.objectData.forEach(data => {
      if (!data.regions) return;
      data.regions.forEach(dataRegion => {
        if (!this.objectDataByRegion[dataRegion]) this.objectDataByRegion[dataRegion] = [];
        this.objectDataByRegion[dataRegion].push(data);
      });
    });
    // Sort data-by-region by sort key if exists
    if (!this.sortKey) return this.objectDataByRegion;
    for (let [key, value] of Object.entries(this.objectDataByRegion)) {
      value.sort((a, b) => this.genericSortFunction(a, b));
      this.objectDataByRegion[key] = value;
    }
    return this.objectDataByRegion;
  }
}
