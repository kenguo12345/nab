import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  templateUrl: './add-item-modal.component.html'
})
export class AddItemModalComponent {


  key: string;
  title: string;
  @Output() create = new EventEmitter<string>();

  constructor(public bsModalRef: BsModalRef) { }

  save() {
    this.create.emit(this.key);
    this.bsModalRef.hide();
  }
}
