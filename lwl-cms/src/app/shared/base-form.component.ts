import { OnInit, Injectable, OnDestroy } from '@angular/core';
import { ActivatedRoute, UrlSegment, ParamMap } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first } from 'rxjs/operators';
import { ToastrService, ActiveToast } from 'ngx-toastr';
import { RegionSelectorComponent } from '../locale/region-selector/region-selector.component';
import { GenericDataSelectorComponent } from '../locale/generic-data-selector/generic-data-selector.component';
import * as _ from 'lodash';
import { Component } from '@angular/core';
import { combineLatest } from 'rxjs';
import { environment } from 'src/environments/environment';

export type FieldDefinition = {
  name: string,
  type: 'string' | 'locale' | 'asset' | 'regions' | 'objectPicker' | 'localeObjectPicker' | 'dropdown' | 'localisedObjectPicker',
  textFieldType?: 'text' | 'textarea',
  defaultValue?: string | Array<string>,
  valueFormatter?: (v: string) => any,
  pickerDocType?: string,
  dropdownValues?: { [s: number]: string }
};

export interface FileAssets {
  [key: string]: {
    [region: string]: {
      [language: string]: string
    }
  };
}

export class BaseFormComponent implements OnInit, OnDestroy {

  assets: FileAssets;
  form: FormGroup;
  id: string;
  doctype: string;
  key: string;
  item: any;
  localeTextModal: BsModalRef;
  regionSelector: BsModalRef;
  fields: FieldDefinition[];
  formChangedWarning: ActiveToast<Object>;
  saveDisabled: boolean;
  saveLabel: string;

  labelSelector: BsModalRef;

  constructor(
    protected route: ActivatedRoute,
    protected fb: FormBuilder,
    protected data: DataService,
    protected toastr: ToastrService,
    protected modalService: BsModalService) { }

  ngOnInit(): void {
    const formFields: object = {
      key: ['']
    };
    this.fields.forEach(x => {
      // Assets are constructed separately. So ignore them in the list of fields.
      if (x.type !== 'asset') { formFields[x.name] = ['']; }
    });
    this.form = this.fb.group(formFields);

    combineLatest(
      this.route.parent.url,
      this.route.parent.paramMap
    ).subscribe(([url, params]: [UrlSegment[], ParamMap]) => {
      this.doctype = url[0].path;
      this.key = params.get('key');
      this.id = `${this.doctype}:${this.key}`;
      this.data.getItem(this.id).subscribe(d => {
        this.item = d;
        this.fillForm();
      });

      this.data.getAssets(this.id).subscribe((assets: FileAssets) => {
        this.assets = assets;
      });
      // this.testLabelSelect('labels');
    });

  }

  selectDataObject(key: string, dataDocType: string): void {
    if (!dataDocType) {
      console.error('No data doc type set on field.');
      return;
    }
    const selectedData = this.form.contains(key) && this.form.get(key).value;
    GenericDataSelectorComponent
    this.labelSelector = this.modalService.show(
      GenericDataSelectorComponent, {
      initialState: { dataDocType, key, selectedData },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });
    this.labelSelector.content.update.pipe(first()).subscribe((update: string[]) => {
      const control = this.form.get(key);
      control.setValue(update.join(', '));
      control.markAsDirty();
    });
  }

  ngOnDestroy(): void {
    if (this.formChangedWarning) {
      this.toastr.clear(this.formChangedWarning.toastId);
      this.formChangedWarning = null;
    }
  }

  onChanges(): void {
    this.form.valueChanges.subscribe(val => {
      if (!this.formChangedWarning) {
        this.formChangedWarning = this.toastr.warning('Unsaved Changes', '', { timeOut: 0, extendedTimeOut: 0 });
      }
    });
  }

  valueChange(update) {
    this.item = { ...this.item, ...update };
    this.fillForm();
  }

  getAsset(assetKey: string) {
    return _.get(this.assets, assetKey);
  }

  fillForm() {
    const values = {};
    for (const c of Object.keys(this.form.controls)) {
      const val = this.item[c];

      const value = _.isPlainObject(val)
        ? val['-']['-']
        : _.isArray(val)
          ? val.join(', ')
          : val;

      values[c] = value || "";
    }
    this.form.reset();
    this.form.patchValue(values);
    if (!environment.formsEnabled) this.form.disable();
    this.onChanges();
  }

  save() {
    if (this.form.valid && this.form.dirty) {
      for (const key of Object.keys(this.form.value).filter(k => k !== 'key')) {
        const field = this.fields.find(f => f.name === key);
        if (!field) { console.error(`Associated field does not exist in fields array for ${key}`); }
        let controlValue = this.form.controls[key].value;
        if (field.valueFormatter && controlValue) { controlValue = field.valueFormatter(controlValue); }

        if (field.type === 'locale' || field.type === 'localisedObjectPicker') {
          // If initting for the first time
          if (!_.isObject(this.item[key])) {
            this.item[key] = { '-': { '-': '' } };
          }
          // If there is a controlValue then the field was edited in the frontend(not the modal)
          // If so replace the default vaue with this value so it is not lost
          if (controlValue) this.item[key]['-']['-'] = controlValue;
        } else {
          if (!controlValue && field.defaultValue) controlValue = field.defaultValue;
          this.item[key] = controlValue;
        }
      }
      this.saveItem();
    }
  }

  saveItem() {
    this.data.saveItem(this.id, this.item).subscribe(() => {
      this.form.markAsPristine();
      this.form.markAsUntouched();
      if (this.formChangedWarning) {
        this.toastr.clear(this.formChangedWarning.toastId);
        this.formChangedWarning = null;
      }
      this.toastr.success('Experience saved successfully.');
    });
  }

  selectRegions(key: string) {
    const regions = this.form.contains(key) && this.form.get(key).value;
    this.regionSelector = this.modalService.show(
      RegionSelectorComponent, {
      initialState: { key, selectedRegions: regions },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });
    this.regionSelector.content.update.pipe(first()).subscribe((update: string[]) => {
      const control = this.form.get(key);
      control.setValue(update.join(', '));
      control.markAsDirty();
    });
  }
}
