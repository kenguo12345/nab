import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ModalModule, TypeaheadModule, TooltipModule } from 'ngx-bootstrap';
import { MsAdalAngular6Module, AuthenticationGuard } from 'microsoft-adal-angular6';
import { environment } from 'src/environments/environment';

import { AppComponent } from './app.component';
import { AppConfigNavComponent } from './app-config/nav/app-config-nav.component';
import { AppConfigEditComponent } from './app-config/edit/app-config-edit.component';
import { LocaleTextListComponent } from './locale/text-list/text-list.component';
import { LocaleTextModalComponent } from './locale/text-modal/text-modal.component';
import { LocaleFileListComponent } from './locale/file-list/file-list.component';
import { LocaleFileModalComponent } from './locale/file-modal/file-modal.component';
import { ExperienceNavComponent } from './experience/nav/experience-nav.component';
import { ExperienceEditComponent } from './experience/edit/experience-edit.component';
import { ApiKeyInterceptor } from './services/api-key-interceptor';
import { LocaleFileControlComponent } from './locale/file-control/file-control.component';
import { LocaleTextControlComponent } from './locale/text-control/text-control.component';
import { LocalisedDataSelectorComponent } from './locale/localised-data-selector/localised-data-selector.component';
import { RegionSelectorComponent } from './locale/region-selector/region-selector.component';
import { DropdownControlComponent } from './locale/dropdown-control/dropdown-control.component';
import { GenericDataSelectorComponent } from './locale/generic-data-selector/generic-data-selector.component';

import { PromotionNavComponent } from './promotion/nav/promotion-nav.component';
import { PromotionEditComponent } from './promotion/edit/promotion-edit.component';

import { VarietalNavComponent } from './varietal/nav/varietal-nav.component';
import { VarietalEditComponent } from './varietal/edit/varietal-edit.component';

import { LabelNavComponent } from './label/nav/label-nav.component';
import { LabelEditComponent } from './label/edit/label-edit.component';

import { RecipeNavComponent } from './recipe/nav/recipe-nav.component';
import { RecipeEditComponent } from './recipe/edit/recipe-edit.component';

import { ThemeNavComponent } from './theme/nav/theme-nav.component';
import { ThemeEditComponent } from './theme/edit/theme-edit.component';

import { LanguageNavComponent } from './language/nav/language-nav.component';
import { LanguageEditComponent } from './language/edit/language-edit.component';
import { AddItemModalComponent } from './shared/add-item-modal/add-item-modal.component';

import { TastingNoteNavComponent } from './tasting-note/nav/tasting-note-nav.component';
import { TastingNoteEditComponent } from './tasting-note/edit/tasting-note-edit.component';

import { RegionGroupNavComponent } from './region-group/nav/region-group-nav.component';
import { RegionGroupEditComponent } from './region-group/edit/region-group-edit.component';

import { GenericListComponent } from './shared/generic-list/generic-list.component';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { VarietalDataListComponent } from './varietal-data/varietal-data-list/varietal-data-list.component';

import { VarietalInfoNavComponent } from './varietal-data/varietal-info/nav/varietal-info-nav.component';
import { VarietalInfoEditComponent } from './varietal-data/varietal-info/edit/varietal-info-edit.component';

import { VarietalPairingNavComponent } from './varietal-data/varietal-pairing/nav/varietal-pairing-nav.component';
import { VarietalPairingEditComponent } from './varietal-data/varietal-pairing/edit/varietal-pairing-edit.component';

import { VarietalTipFactNavComponent } from './varietal-data/varietal-tip-fact/nav/varietal-tip-fact-nav.component';
import { VarietalTipFactEditComponent } from './varietal-data/varietal-tip-fact/edit/varietal-tip-fact-edit.component';

import { VarietalTastingNavComponent } from './varietal-data/varietal-tasting/nav/varietal-tasting-nav.component';
import { VarietalTastingEditComponent } from './varietal-data/varietal-tasting/edit/varietal-tasting-edit.component';

import { VarietalExtraIconNavComponent } from './varietal-data/varietal-extra-icons/nav/varietal-extra-icon-nav.component';
import { VarietalExtraIconEditComponent } from './varietal-data/varietal-extra-icons/edit/varietal-extra-icon-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    AppConfigEditComponent,
    AppConfigNavComponent,
    LocaleTextListComponent,
    LocaleTextModalComponent,
    LocaleFileListComponent,
    LocaleFileModalComponent,

    ExperienceNavComponent,
    ExperienceEditComponent,

    PromotionNavComponent,
    PromotionEditComponent,

    VarietalNavComponent,
    VarietalEditComponent,

    LabelNavComponent,
    LabelEditComponent,

    ThemeNavComponent,
    ThemeEditComponent,

    RecipeNavComponent,
    RecipeEditComponent,

    LanguageNavComponent,
    LanguageEditComponent,

    LocaleFileControlComponent,
    LocaleTextControlComponent,
    LocalisedDataSelectorComponent,
    DropdownControlComponent,
    RegionSelectorComponent,
    GenericDataSelectorComponent,
    AddItemModalComponent,

    TastingNoteNavComponent,
    TastingNoteEditComponent,

    RegionGroupNavComponent,
    RegionGroupEditComponent,

    GenericListComponent,
    VarietalDataListComponent,

    VarietalInfoNavComponent,
    VarietalInfoEditComponent,

    VarietalPairingNavComponent,
    VarietalPairingEditComponent,

    VarietalTipFactNavComponent,
    VarietalTipFactEditComponent,

    VarietalTastingNavComponent,
    VarietalTastingEditComponent,

    VarietalExtraIconNavComponent,
    VarietalExtraIconEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({ positionClass: 'toast-bottom-right', enableHtml: true }),
    NgxSpinnerModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    TooltipModule.forRoot(),
    MsAdalAngular6Module.forRoot({
      tenant: environment.aadAuth.tenant,
      clientId: environment.aadAuth.clientId,
      redirectUri: window.location.origin,
      navigateToLoginRequestUrl: false,
      cacheLocation: 'sessionStorage',
    }),
    TabsModule.forRoot(),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiKeyInterceptor,
      multi: true
    },
    AuthenticationGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LocaleTextModalComponent,
    LocaleFileModalComponent,
    RegionSelectorComponent,
    GenericDataSelectorComponent,
    AddItemModalComponent
  ]
})
export class AppModule { }
