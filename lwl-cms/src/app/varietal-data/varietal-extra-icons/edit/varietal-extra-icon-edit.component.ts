import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './varietal-extra-icon-edit.component.html',
})
export class VarietalExtraIconEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    { name: 'name', type: 'locale', defaultValue: '' },
    { name: 'extraIcon.png', type: 'asset' }
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
    super(route, fb, data, toastr, modalService);
  }
}
