import { Component } from '@angular/core';

@Component({
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item">
          <a class="nav-link" routerLink="edit" routerLinkActive="active">Edit</a>
      </li>
    </ul>

    <router-outlet></router-outlet>`
})
export class VarietalTastingNavComponent { }
