import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './varietal-info-edit.component.html',
})
export class VarietalInfoEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    {
      name: 'regions', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: ['-']
    },
    {
      name: 'pairingKeys',
      type: 'localisedObjectPicker',
      pickerDocType: 'varietal-pairing',
      defaultValue: ['-']
    },
    { name: 'pairingBlurb', textFieldType: 'textarea', type: 'locale', defaultValue: '' },
    {
      name: 'tastingKeys',
      type: 'objectPicker',
      pickerDocType: 'tasting-note',
      valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(v => v.trim()),
      defaultValue: []
    },
    {
      name: 'tastingIconKeys',
      type: 'objectPicker',
      pickerDocType: 'varietal-tasting',
      valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(v => v.trim()),
      defaultValue: []
    },
    {
      name: 'tipFactKeys',
      type: 'objectPicker',
      pickerDocType: 'varietal-tip-fact',
      valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(v => v.trim()),
      defaultValue: []
    },
    { name: 'tipFactBlurb', textFieldType: 'textarea', type: 'locale', defaultValue: '' },
    {
      name: 'extraIcon',
      type: 'localisedObjectPicker',
      pickerDocType: 'varietal-extra-icon',
      defaultValue: ['-']
    }
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
    super(route, fb, data, toastr, modalService);
  }
}
