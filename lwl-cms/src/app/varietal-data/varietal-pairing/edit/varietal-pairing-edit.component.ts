import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './varietal-pairing-edit.component.html',
})
export class VarietalPairingEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    { name: 'name', type: 'locale', defaultValue: '' },
    { name: 'description', type: 'locale', defaultValue: '' },
    { name: 'pairingIcon.png', type: 'asset'}
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
      super(route, fb, data, toastr, modalService);
    }
}
