import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { AddItemModalComponent } from 'src/app/shared/add-item-modal/add-item-modal.component';
import { first } from 'rxjs/operators';
import { LocaleService, Locality } from 'src/app/services/locale.service';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './varietal-data-list.component.html',
  styleUrls: ['./varietal-data-list.component.css']
})
export class VarietalDataListComponent implements OnInit {
  envName: string = environment.envName;
  formKeys: any;
  filter: string;
  type: string;
  addModal: BsModalRef;
  objectData: Array<any>;
  objectDataByRegion: Object;
  showListByRegionTab: boolean;

  constructor(
    private route: ActivatedRoute,
    private data: DataService,
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private locale: LocaleService) {
      this.type = this.route.snapshot.data["type"];
      this.showListByRegionTab = false;
    }

  ngOnInit() {
   
  }
}
