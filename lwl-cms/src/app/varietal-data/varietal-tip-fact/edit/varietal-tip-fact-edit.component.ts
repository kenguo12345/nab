import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';
import { TipFactType } from 'src/app/shared/data-types/data-types';

@Component({
  templateUrl: './varietal-tip-fact-edit.component.html',
})
export class VarietalTipFactEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    { name: 'type', type: 'dropdown', dropdownValues: TipFactType, defaultValue: TipFactType.Point},
    { name: 'name', type: 'locale', defaultValue: '' },
    { name: 'iconText', type: 'locale', defaultValue: '' },
    { name: 'longText', type: 'locale', defaultValue: '' },
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
      super(route, fb, data, toastr, modalService);
    }
}
