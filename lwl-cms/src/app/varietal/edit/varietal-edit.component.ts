import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './varietal-edit.component.html',
})
export class VarietalEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    {
      name: 'regions', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
        defaultValue: ['-']
    },
    { name: 'brandName', type: 'locale', defaultValue: '' },
    { name: 'name', type: 'locale', defaultValue: '' },
    { name: 'vintage', type: 'locale', defaultValue: '' },
    { name: 'wineRegion', type: 'locale', defaultValue: '' },

    { name: 'bottleImage.png', type: 'asset' },
    { name: 'icon.png', type: 'asset' },
    {
      name: 'tastingNoteKey',
      type: 'objectPicker',
      pickerDocType: 'tasting-note',
      valueFormatter: (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: []
    },
    {
      name: 'recipeKey',
      type: 'objectPicker',
      pickerDocType: 'recipe',
      valueFormatter: (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: []
     },
    { name: 'buyNowUrl', type: 'locale', defaultValue: '' },
    {
      name: 'varietalInfoKey',
      type: 'objectPicker',
      pickerDocType: 'varietal-info',
      valueFormatter: (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: []
     }
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
      super(route, fb, data, toastr, modalService);
    }
}
