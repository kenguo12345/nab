import { Component } from '@angular/core';

@Component({
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item">
          <a class="nav-link" routerLink="edit" routerLinkActive="active">Edit</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" routerLink="text" routerLinkActive="active">Text</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" routerLink="files" routerLinkActive="active">Files</a>
      </li>
    </ul>

    <router-outlet></router-outlet>`
})
export class AppConfigNavComponent { }
