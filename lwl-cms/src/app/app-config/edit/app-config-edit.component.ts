import { Component, Injectable, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';
import { BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute, UrlSegment, ParamMap } from '@angular/router';
import { combineLatest } from 'rxjs';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';

@Component({
  templateUrl: './app-config-edit.component.html'
})
@Injectable()
export class AppConfigEditComponent extends BaseFormComponent implements OnInit {

  fields: FieldDefinition[] = [
    { name: 'minBuildAndroid', type: 'string' },
    { name: 'minBuildIos', type: 'string' },

    { name: 'updateTitle', type: 'locale' },
    { name: 'updateDescription', type: 'locale' },
    { name: 'updateButtonText', type: 'locale' },
    { name: 'updateLinkAndroid', type: 'locale' },
    { name: 'updateLinkIos', type: 'locale' },

    { name: 'drinkLogo.png', type: 'asset' },
    { name: 'showMessage', type: 'locale' },
    { name: 'disableApp', type: 'locale' }
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService
  ) {
    super(route, fb, data, toastr, modalService);
  }

  ngOnInit() {
    const formFields: object = {
      key: ['']
    };
    this.fields.forEach(x => {
      if (x.type === 'locale' || x.type === 'string') { formFields[x.name] = ['']; }
    });
    this.form = this.fb.group(formFields);

    combineLatest(
      this.route.parent.url,
      this.route.parent.paramMap
    ).subscribe(([url]: [UrlSegment[], ParamMap]) => {
      this.doctype = url[0].path;
      this.id = `${this.doctype}`;
      this.data.getItem(this.id).subscribe(d => {
        this.item = d;
        this.fillForm();
      });

      this.data.getAssets(this.id).subscribe(assets => {
        this.assets = assets;
      });
    });
  }
}
