import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppConfigEditComponent } from '../app-config/edit/app-config-edit.component';

@Injectable({
  providedIn: 'root'
})
export class FormEditGuard implements CanDeactivate<AppConfigEditComponent> {
  canDeactivate(
    component: AppConfigEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): boolean {

    return component.form.dirty
      ? confirm('Navigate away and lose all changes?')
      : true;
  }
}
