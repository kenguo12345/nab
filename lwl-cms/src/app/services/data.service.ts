import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class DataService {

    constructor(private http: HttpClient) { }

    getAssets(itemId: string, folder?: string): Observable<any> {
        const url = `${environment.apiUrl}/v1/admin/assets/${itemId}`;
        const options = folder ? { params: new HttpParams().set('folder', folder) } : {};
        return this.http.get<any>(url, options);
    }

    uploadAsset(itemId: string, key: string, file: File, folder?: string, region?: string, language?: string) {
        const url = `${environment.apiUrl}/v1/admin/assets/${itemId}`;

        const formData: FormData = new FormData();
        formData.set('assetFile', file, key);

        let params = new HttpParams();
        if (folder) { params = params.set('folder', folder); }
        if (region) { params = params.set('region', region); }
        if (language) { params = params.set('language', language); }
        console.log(url, file, key, folder, region, language);

        return this.http.post(url, formData, { params });
    }

    deleteAsset(itemId: string, key: string, folder?: string, region?: string, language?: string) {
        const url = `${environment.apiUrl}/v1/admin/assets/${itemId}/${key}`;

        let params = new HttpParams();
        if (folder) { params = params.set('folder', folder); }
        if (region) { params = params.set('region', region); }
        if (language) { params = params.set('language', language); }

        return this.http.delete(url, { params });
    }

    deleteAssetAll(itemId: string, key: string, folder?: string) {
        const url = `${environment.apiUrl}/v1/admin/assets/${itemId}/${key}/all`;
        let params = new HttpParams();
        if (folder) { params = params.set('folder', folder); }

        return this.http.delete(url, { params });
    }

    getList(type: string): Observable<any> {
        const url = `${environment.apiUrl}/v1/admin/list/${type}`;
        return this.http.get<any>(url);
    }

    getItem(id: string): Observable<any> {
        const url = `${environment.apiUrl}/v1/admin/item/${id}`;
        return this.http.get<any>(url);
    }

    saveItem(id: string, item: any): Observable<any> {
        const url = `${environment.apiUrl}/v1/admin/item/${id}`;
        return this.http.put<any>(url, item);
    }

    deleteItem(id: string): Observable<any> {
        const url = `${environment.apiUrl}/v1/admin/item/${id}`;
        return this.http.delete<any>(url);
    }
}

export interface AppConfig {
    minBuildAndroid: string;
    minBuildiOS: string;
    updateButtonText: string;
    updateDesc: string;
    updateLinkAndroid: string;
    updateLinkiOS: string;
    updateTitle: string;
    textLocalisations: any;
}
