import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, UrlSegment } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { combineLatest } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { LocaleTextModalComponent } from 'src/app/locale/text-modal/text-modal.component';
import { take } from 'rxjs/operators';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';
import { VarietalInfoMode } from 'src/app/shared/data-types/data-types';

@Component({
  templateUrl: './label-edit.component.html',
})
export class LabelEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    {
      name: 'regions', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
        defaultValue: ['-']
    },
    { name: 'name', type: 'locale' },
    { name: 'brand', type: 'locale' },
    { name: 'varietals',
    type: 'objectPicker',
    pickerDocType: 'varietal',
      valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(v => v.trim()),
        defaultValue: []
    },
    { name: 'hideInfoPopup', type: 'locale' },
    { name: 'varietalInfoMode', type: 'dropdown', dropdownValues: VarietalInfoMode, defaultValue: VarietalInfoMode.Screen},
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
      super(route, fb, data, toastr, modalService);
    }
}
