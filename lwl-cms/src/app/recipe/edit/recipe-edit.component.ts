import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './recipe-edit.component.html',
})
export class RecipeEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    {
      name: 'regions', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
        defaultValue: ['-']
    },
    { name: 'name', type: 'locale' },
    { name: 'headerImage.png', type: 'asset' },
    { name: 'serves', type: 'locale', textFieldType: 'textarea' }
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
      super(route, fb, data, toastr, modalService);
    }
}
