import { Component } from '@angular/core';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  envName: string = environment.envName;
  constructor(public adal: MsAdalAngular6Service) { }

  get authEnabled(): boolean {
    return environment.aadAuth.enabled;
  }

  logout() {
    this.adal.logout();
  }

  get formsEnabled(): boolean {
    return environment.formsEnabled;
  }
}
