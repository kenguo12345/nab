import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';
import * as _ from 'lodash';

@Component({
  templateUrl: './experience-edit.component.html',
})
export class ExperienceEditComponent extends BaseFormComponent {

  fields: FieldDefinition[] = [
    {
      name: 'regions', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
        defaultValue: ['-']
    },

    { name: 'name', type: 'locale' },
    { name: 'displayName', type: 'locale' },
    { name: 'build', type: 'string' },
    { name: 'version', type: 'string' },
    { name: 'status', type: 'string' },
    { name: 'size', type: 'string' },
    { name: 'labels', type: 'objectPicker', valueFormatter:
      (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: [],
      pickerDocType: 'label'
    },

    { name: 'brandCopy', type: 'locale', textFieldType: 'textarea' },
    {
      name: 'capture', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim())
    },
    { name: 'captureName', type: 'locale' },
    { name: 'captureCopyA', type: 'locale', textFieldType: 'textarea' },
    { name: 'captureCopyB', type: 'locale', textFieldType: 'textarea' },

    { name: 'privacyUrl', type: 'locale' },
    { name: 'websiteUrl', type: 'locale' },
    { name: 'websiteLabel', type: 'locale' },

    { name: 'icon.png', type: 'asset' },
    { name: 'assetBundleIos', type: 'asset' },
    { name: 'assetBundleAndroid', type: 'asset' },
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
    super(route, fb, data, toastr, modalService);
  }
}
