import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEditGuard } from 'src/app/services/form-edit.guard';
import { environment } from 'src/environments/environment';
import { AppConfigNavComponent } from './app-config/nav/app-config-nav.component';
import { AppConfigEditComponent } from './app-config/edit/app-config-edit.component';
import { LocaleTextListComponent } from './locale/text-list/text-list.component';
import { LocaleFileListComponent } from './locale/file-list/file-list.component';

import { ExperienceNavComponent } from './experience/nav/experience-nav.component';
import { ExperienceEditComponent } from './experience/edit/experience-edit.component';

import { PromotionNavComponent } from './promotion/nav/promotion-nav.component';
import { PromotionEditComponent } from './promotion/edit/promotion-edit.component';

import { VarietalNavComponent } from './varietal/nav/varietal-nav.component';
import { VarietalEditComponent } from './varietal/edit/varietal-edit.component';

import { GenericListComponent } from './shared/generic-list/generic-list.component';

import { LabelNavComponent } from './label/nav/label-nav.component';
import { LabelEditComponent } from './label/edit/label-edit.component';

import { LanguageNavComponent } from './language/nav/language-nav.component';
import { LanguageEditComponent } from './language/edit/language-edit.component';

import { ThemeEditComponent } from './theme/edit/theme-edit.component';
import { ThemeNavComponent } from './theme/nav/theme-nav.component';

import { RecipeNavComponent } from './recipe/nav/recipe-nav.component';
import { RecipeEditComponent } from './recipe/edit/recipe-edit.component';

import { TastingNoteNavComponent } from './tasting-note/nav/tasting-note-nav.component';
import { TastingNoteEditComponent } from './tasting-note/edit/tasting-note-edit.component';

import { RegionGroupNavComponent } from './region-group/nav/region-group-nav.component';
import { RegionGroupEditComponent } from './region-group/edit/region-group-edit.component';

// Varietal Data
import { VarietalDataListComponent } from './varietal-data/varietal-data-list/varietal-data-list.component';

import { VarietalInfoNavComponent } from './varietal-data/varietal-info/nav/varietal-info-nav.component';
import { VarietalInfoEditComponent } from './varietal-data/varietal-info/edit/varietal-info-edit.component';

import { VarietalPairingNavComponent } from './varietal-data/varietal-pairing/nav/varietal-pairing-nav.component';
import { VarietalPairingEditComponent } from './varietal-data/varietal-pairing/edit/varietal-pairing-edit.component';

import { VarietalTipFactNavComponent } from './varietal-data/varietal-tip-fact/nav/varietal-tip-fact-nav.component';
import { VarietalTipFactEditComponent } from './varietal-data/varietal-tip-fact/edit/varietal-tip-fact-edit.component';

import { VarietalTastingNavComponent } from './varietal-data/varietal-tasting/nav/varietal-tasting-nav.component';
import { VarietalTastingEditComponent } from './varietal-data/varietal-tasting/edit/varietal-tasting-edit.component';

import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { VarietalExtraIconNavComponent } from './varietal-data/varietal-extra-icons/nav/varietal-extra-icon-nav.component';
import { VarietalExtraIconEditComponent } from './varietal-data/varietal-extra-icons/edit/varietal-extra-icon-edit.component';

let routes: Routes =
  [
    {
      path: 'app-config', component: AppConfigNavComponent, data: { title: 'App' }, children: [
        { path: 'edit', component: AppConfigEditComponent, canDeactivate: [FormEditGuard] },
        { path: 'text', component: LocaleTextListComponent, data: { id: 'app-config' } },
        { path: 'files', component: LocaleFileListComponent, data: { id: 'app-config' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'experience', component: GenericListComponent, data: { title: 'Experiences', type: 'experience' } },
    {
      path: 'experience/:key', component: ExperienceNavComponent, children: [
        { path: 'edit', component: ExperienceEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'experience' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'experience' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'language', component: GenericListComponent, data: { title: 'Languages', type: 'language', sortKey: 'order' } },
    {
      path: 'language/:key', component: LanguageNavComponent, children: [
        { path: 'edit', component: LanguageEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'language' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'language' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'label', component: GenericListComponent, data: { title: 'Labels', type: 'label' } },
    {
      path: 'label/:key', component: LabelNavComponent, children: [
        { path: 'edit', component: LabelEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'label' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'label' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'promotion', component: GenericListComponent, data: { title: 'Promotions', type: 'promotion', sortKey: 'order' } },
    {
      path: 'promotion/:key', component: PromotionNavComponent, children: [
        { path: 'edit', component: PromotionEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'promotion' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'promotion' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'varietal', component: GenericListComponent, data: { title: 'Varietals', type: 'varietal' } },
    {
      path: 'varietal/:key', component: VarietalNavComponent, children: [
        { path: 'edit', component: VarietalEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'varietal' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'varietal' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'tasting-note', component: GenericListComponent, data: { title: 'Tasting Notes', type: 'tasting-note' } },
    {
      path: 'tasting-note/:key', component: TastingNoteNavComponent, children: [
        { path: 'edit', component: TastingNoteEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'tasting-note' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'tasting-note' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'recipe', component: GenericListComponent, data: { title: 'Recipes', type: 'recipe' } },
    {
      path: 'recipe/:key', component: RecipeNavComponent, children: [
        { path: 'edit', component: RecipeEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'recipe' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'recipe' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'theme', component: GenericListComponent, data: { title: 'Themes', type: 'theme' } },
    {
      path: 'theme/:key', component: ThemeNavComponent, children: [
        { path: 'edit', component: ThemeEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'theme' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'theme' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'region-group', component: GenericListComponent, data: { title: 'Region Groups', type: 'region-group' } },
    {
      path: 'region-group/:key', component: RegionGroupNavComponent, children: [
        { path: 'edit', component: RegionGroupEditComponent },
        { path: 'text', component: LocaleTextListComponent, data: { doctype: 'region-group' } },
        { path: 'files', component: LocaleFileListComponent, data: { doctype: 'region-group' } },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'varietal-data', component: VarietalDataListComponent, data: { title: 'Varietal HUD Data', type: 'varietal-data' } },

    { path: 'varietal-info', component: GenericListComponent, data: { title: 'Varietal HUD Data', type: 'varietal-info' } },
    {
      path: 'varietal-info/:key', component: VarietalInfoNavComponent, children: [
        { path: 'edit', component: VarietalInfoEditComponent },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },

    { path: 'varietal-pairing', component: GenericListComponent, data: { title: 'Varietal HUD Data', type: 'varietal-pairing' } },
    {
      path: 'varietal-pairing/:key', component: VarietalPairingNavComponent, children: [
        { path: 'edit', component: VarietalPairingEditComponent },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'varietal-tip-fact', component: GenericListComponent, data: { title: 'Varietal HUD Data', type: 'varietal-tip-fact' } },
    {
      path: 'varietal-tip-fact/:key', component: VarietalTipFactNavComponent, children: [
        { path: 'edit', component: VarietalTipFactEditComponent },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'varietal-tasting', component: GenericListComponent, data: { title: 'Varietal HUD Data', type: 'varietal-tasting' } },
    {
      path: 'varietal-tasting/:key', component: VarietalTastingNavComponent, children: [
        { path: 'edit', component: VarietalTastingEditComponent },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: 'varietal-extra-icon', component: GenericListComponent, data: { title: 'Varietal HUD Data', type: 'varietal-extra-icon' } },
    {
      path: 'varietal-extra-icon/:key', component: VarietalExtraIconNavComponent, children: [
        { path: 'edit', component: VarietalExtraIconEditComponent },
        { path: '', redirectTo: 'edit', pathMatch: 'full' }
      ]
    },
    { path: '', redirectTo: 'app-config', pathMatch: 'full' },
  ];

if (environment.aadAuth.enabled) {
  routes = [{
    path: '', canActivate: [AuthenticationGuard], children: routes
  }];
}

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
