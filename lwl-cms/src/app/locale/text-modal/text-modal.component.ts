import { Component, OnInit, ViewChildren, QueryList, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef, TypeaheadMatch, BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, FormArray, Validators, AbstractControl } from '@angular/forms';
import { LocaleService } from 'src/app/services/locale.service';
import * as _ from 'lodash';
import { environment } from 'src/environments/environment';
import { GenericDataSelectorComponent } from '../generic-data-selector/generic-data-selector.component';
import { first } from 'rxjs/operators';

class Localisation {
  region: string;
  regionCode: string;
  language: string;
  languageCode: string;
  value: string;
  public constructor(init?: Partial<Localisation>) { Object.assign(this, init); }
}

interface TextLocalisations {
  [key: string]: string | { [key: string]: string };
}

interface TextLocalisationsUpdate {
  [key: string]: TextLocalisations;
}

@Component({
  templateUrl: './text-modal.component.html'
})
export class LocaleTextModalComponent implements OnInit, AfterViewInit {

  @ViewChildren('regionElement') regionElement: QueryList<ElementRef>;
  @Output() update = new EventEmitter<TextLocalisationsUpdate>();

  readonly wildcard: string = '-';

  key: string;
  textLocalisations: TextLocalisations = {}
  textFieldType: string;
  pickerDocType: string;

  labelSelector: BsModalRef;

  isNew: boolean;
  form: FormGroup;

  get localisations(): FormArray { return this.form.get('localisations') as FormArray; }

  constructor(
    public bsModalRef: BsModalRef,
    protected modalService: BsModalService,
    private fb: FormBuilder,
    public locale: LocaleService) { }

  ngOnInit() {
    this.isNew = !this.key;
    this.textLocalisations = this.textLocalisations || {};
    const defaultValue = _.get(this.textLocalisations, [this.wildcard, this.wildcard]);
    this.form = this.fb.group({
      key: [this.key, Validators.required],
      default: [defaultValue, Validators.nullValidator],
      localisations: this.fb.array([]),
    });
    this.parseValuesToObject();
    this.initLocalisations();
    if (!environment.formsEnabled) this.form.disable();
  }

  ngAfterViewInit() {
    this.regionElement.changes.subscribe(() => {
      if (this.regionElement.last) {
        this.regionElement.last.nativeElement.focus();
      }
    });
  }

  parseValuesToObject() {
    if (!_.isArray(this.textLocalisations)) return;
    this.textLocalisations = {
      '-': {
        '-': this.textLocalisations.join(',')
      }
    }
  }

  initLocalisations() {
    const regionCodes = Object.keys(this.textLocalisations);
    for (const regionCode of regionCodes) {
      const regionItem = this.locale.regions.filter(c => c.code === regionCode)[0];
      const languageCodes = Object.keys(this.textLocalisations[regionCode]);
      for (const languageCode of languageCodes) {
        if (regionCode === this.wildcard && languageCode === this.wildcard) { continue; }
        const languageItem = this.locale.languages.filter(c => c.code === languageCode)[0];
        this.addLocalisation({
          region: regionItem.name,
          regionCode: regionItem.code,
          language: languageItem.name,
          languageCode: languageItem.code,
          value: this.textLocalisations[regionCode][languageCode],
        });
      }
    }
  }

  loadPickerForDefaultField() {
    const control = this.form.controls['default'];
    this.loadPicker('default', control);
  }

  loadPickerForLocalField(key: string) {
    const control = this.localisations.controls[key].get('value');
    this.loadPicker(key, control);
  }

  loadPicker(key: string, control: AbstractControl) {
    const currentData = control.value;
    GenericDataSelectorComponent
    this.labelSelector = this.modalService.show(
      GenericDataSelectorComponent, {
      initialState: { dataDocType: this.pickerDocType, key, selectedData: currentData },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });
    this.labelSelector.content.update.pipe(first()).subscribe((update: string[]) => {
      control.setValue(update.join(','));
      control.markAsDirty();
    });
  }

  addLocalisation(localisation?: Localisation) {
    const isAddNew = !localisation;
    if (isAddNew) {
      const anyLanguage = this.locale.languages.filter(l => l.code === this.wildcard)[0];
      localisation = new Localisation({
        language: anyLanguage.name,
        languageCode: anyLanguage.code
      });
    }

    (this.localisations as FormArray).push(this.fb.group({
      region: [localisation.region, Validators.compose([Validators.required, regionValidator])],
      regionCode: [localisation.regionCode],
      language: [localisation.language, Validators.compose([Validators.required, languageValidator])],
      languageCode: [localisation.languageCode],
      value: [localisation.value, Validators.nullValidator],
    }));
  }

  deleteLocalisation(index: number) {
    (this.localisations as FormArray).removeAt(index);
  }

  onRegionSelect(match: TypeaheadMatch, index: number): void {
    const value = match ? match.item.code : '';
    this.form.get(`localisations.${index}.regionCode`).setValue(value);
    this.form.get(`localisations.${index}.region`).updateValueAndValidity();
  }

  onLanguageSelect(match: TypeaheadMatch, index: number): void {
    const value = match ? match.item.code : '';
    this.form.get(`localisations.${index}.languageCode`).setValue(value);
    this.form.get(`localisations.${index}.language`).updateValueAndValidity();
  }

  close() {
    this.key = this.form.controls.key.value;
    this.update.emit({ [this.key]: this.getTextLocalisations() });
    this.bsModalRef.hide();
  }

  getTextLocalisations(): TextLocalisations {
    return (this.localisations.controls as FormGroup[])
      .map(c => {
        return {
          regionCode: c.controls.regionCode.value,
          languageCode: c.controls.languageCode.value,
          value: c.controls.value.value,
        };
      })
      .reduce((a, v) => {
        const language = { [v.languageCode]: v.value };
        a[v.regionCode] = Object.assign(a[v.regionCode] || {}, language);
        return a;
      }, { '-': { '-': this.form.controls.default.value } });
  }

  isInvalid(controlName: string) {
    const control = this.form.get(controlName);
    return control && control.touched && !control.valid;
  }

  isObjectPicker(): boolean {
    return this.pickerDocType !== undefined && this.pickerDocType !== null;
  }
}


// validators

function regionValidator(c: AbstractControl): { [key: string]: boolean } | null {
  const group = c.parent as FormGroup;
  if (!group) { return null; }
  return group.controls.regionCode.value ? null : { hasRegionCode: true };
}

function languageValidator(c: AbstractControl): { [key: string]: boolean } | null {
  const group = c.parent as FormGroup;
  if (!group) { return null; }
  return group.controls.languageCode.value ? null : { hasLanguageCode: true };
}
