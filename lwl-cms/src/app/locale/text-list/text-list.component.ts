import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { take } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { LocaleTextModalComponent } from '../text-modal/text-modal.component';
import { ActivatedRoute, ParamMap, Data } from '@angular/router';

@Component({
  templateUrl: './text-list.component.html',
  styleUrls: ['./text-list.component.css']
})
export class LocaleTextListComponent implements OnInit {

  item: { textLocalisations: any };
  localisationModal: BsModalRef;
  filter: string;

  doctype: string;
  key: string;
  id: string;

  constructor(
    private route: ActivatedRoute,
    private data: DataService,
    private modalService: BsModalService,
    private toastr: ToastrService) { }

  ngOnInit(): void {

    this.route.data.subscribe((data: Data) => {
      if (data.doctype) {
        this.doctype = data.doctype;
        return;
      }
      if (data.id) {
        this.id = data.id;
        this.data.getItem(this.id).subscribe(i => { this.item = i; });
      }
    });

    this.route.parent.paramMap.subscribe((paramMap: ParamMap) => {
      this.key = paramMap.get('key');
      if (this.doctype && this.key) {
        this.id = `${this.doctype}:${this.key}`;
        this.data.getItem(this.id).subscribe(i => { this.item = i; });
      }
    });
  }

  get keys(): string[] {
    return this.item && this.item.textLocalisations ?
      Object.keys(this.item.textLocalisations).sort().filter(v => {
        return this.filter ? v.includes(this.filter) : true;
      }) : [];
  }

  delete(key: string): void {
    if (confirm(`Are you sure you want to delete ${key}`)) {
      delete this.item.textLocalisations[key];
      this.save();
    }
  }

  edit(key?: string): void {
    const textLocalisations = key ? this.item.textLocalisations[key] : {};
    this.localisationModal = this.modalService.show(
      LocaleTextModalComponent, {
      initialState: { key, textLocalisations },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });

    this.localisationModal.content.update.pipe(take(1)).subscribe((update) => {
      this.item.textLocalisations = { ...this.item.textLocalisations, ...update };
      this.save();
    });
  }

  save(): void {
    this.data.saveItem(`${this.id}`, this.item).subscribe(() => {
      this.toastr.success('Saved successfully.');
    });
  }
}
