import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { LocaleService, Locality } from 'src/app/services/locale.service';
import { DataService } from 'src/app/services/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-generic-data-selector',
  templateUrl: './generic-data-selector.component.html',
  styleUrls: ['./generic-data-selector.component.css']
})
export class GenericDataSelectorComponent implements OnInit {
  form: FormGroup;
  allData: any;
  dataDocType: string;
  selectedData: string[];
  filter: string;
  @Output() update = new EventEmitter<string[]>();
  controls: {};

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private data: DataService,
    private locale: LocaleService) { }

  ngOnInit() {
    if (!this.selectedData) { this.selectedData = []; }
    const controls = {};
    console.log(this.dataDocType);
    this.data.getList(this.dataDocType).subscribe((x) => {
      this.allData = x.map(e => {
        return {key: e.key}
      });
      x.map(e => {
        controls[e.key] = this.selectedData.includes(e.key);
      });
      this.form = this.fb.group(controls);
    });
    this.form = this.fb.group(controls);
    if (!environment.formsEnabled) this.form.disable();
  }

  get dataObjects(): any[] {
    return this.allData ? this.allData.sort(this.sortItem).filter(e => {
      return this.filter ? e.key.toLowerCase().includes(this.filter) : [];
    }) : [];
  }

  sortItem(a: any, b: any): number {
    const aKey = a.key.toLowerCase();
    const bKey = b.key.toLowerCase();
    if (aKey < bKey) { return -1; }
    if (aKey > bKey) { return 1; }
    return 0;
  }

  dataKey(key: string): string {
    return this.dataDocType + key;
  }

  save() {
    console.log(this.form.value, this.form.controls);
    const data = Object.keys(this.form.value).filter(i => this.form.controls[i].value);
    this.update.emit(data);
    this.bsModalRef.hide();
  }
}
