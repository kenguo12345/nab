import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { ToastrService } from 'ngx-toastr';
import { LocaleFileModalComponent } from 'src/app/locale/file-modal/file-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-locale-file-control',
  templateUrl: './file-control.component.html'
})
export class LocaleFileControlComponent implements OnInit {

  form: FormGroup;
  assetModal: BsModalRef;
  defaultAssetUrl: string;

  @Input() label: string;
  @Input() key: string;
  @Input() itemId: string;
  @Input() folder: string;

  localisations: string;
  @Input() set asset(localisations: any) {
    if (localisations === undefined) { return; }
    this.localisations = localisations;
    if (this.form) {
      this.defaultAssetUrl = this.localisations['-']['-'];
      const valueControl = this.form.get('value');
      if (valueControl) { valueControl.setValue(this.defaultAssetUrl); }
    }
  }

  constructor(
    private fb: FormBuilder,
    private data: DataService,
    private toastr: ToastrService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.form = this.fb.group({
      value: this.defaultAssetUrl,
      fileInput: '',
      fileInputText: 'Choose file',
      file: '',
    });
    if (!environment.formsEnabled) this.form.disable();
  }

  fileChange(e: any) {
    const file: File = e.target.files[0];
    this.form.get('fileInputText').setValue(file.name);
    this.form.get('file').setValue(file);
  }

  reset(value?: string) {
    this.form.get('file').setValue(undefined);
    this.form.get('fileInput').setValue('');
    this.form.get('fileInputText').setValue('Choose file');
    if (value) { this.form.get('value').setValue(value); }
  }

  upload(e: MouseEvent) {
    e.preventDefault();
    const file = this.form.get('file').value;
    this.data.uploadAsset(this.itemId, this.key, file).subscribe((assetUrl: string) => {
      this.reset(assetUrl);
      this.toastr.success(`File: ${file.name}`, 'Upload file successful');
    });
  }

  edit(key: string): void {
    this.assetModal = this.modalService.show(
      LocaleFileModalComponent, {
      initialState: { id: this.itemId, key, folder: this.folder, fileLocalisations: this.localisations },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

}
