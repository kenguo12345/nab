import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { LocaleTextModalComponent } from '../text-modal/text-modal.component';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dropdown-control',
  templateUrl: './dropdown-control.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: DropdownControlComponent,
    multi: true
  }]
})
export class DropdownControlComponent implements OnInit, ControlValueAccessor {

  @Input() label: string;
  @Input() value: any;
  @Input() key: string;
  @Input() dropdownValues: any;
  @Output() valueChange = new EventEmitter<any>();
  @Input() defaultValue: string;

  isDisabled = false;
  localeTextModal: BsModalRef;

  onChange: any = () => { };
  onTouched: any = () => { };

  constructor(private modalService: BsModalService) { }

  ngOnInit() {}

  writeValue(val: any): void {
    if (!val) return;
    this.defaultValue = val;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
