import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { LocaleTextModalComponent } from '../text-modal/text-modal.component';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-text-control',
  templateUrl: './text-control.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: LocaleTextControlComponent,
    multi: true
  }]
})
export class LocaleTextControlComponent implements OnInit, ControlValueAccessor {

  @Input() label: string;
  @Input() value: any;
  @Input() key: string;
  @Input() textFieldType: string;
  @Output() valueChange = new EventEmitter<any>();

  isDisabled = false;
  defaultValue: string;
  localeTextModal: BsModalRef;

  onChange: any = () => { };
  onTouched: any = () => { };

  constructor(private modalService: BsModalService) { }

  ngOnInit() { }

  writeValue(val: any): void {
    this.defaultValue = val;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  editLocalisations(): void {
    this.localeTextModal = this.modalService.show(
      LocaleTextModalComponent, {
      initialState: { key: this.key, textLocalisations: this.value, textFieldType: this.textFieldType },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });

    this.localeTextModal.content.update.pipe(first()).subscribe((update: any) => {
      this.valueChange.emit(update);
      this.onChange();
    });
  }
}
