import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Data, ParamMap } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { LocaleFileModalComponent } from '../file-modal/file-modal.component';

@Component({
  templateUrl: './file-list.component.html'
})
export class LocaleFileListComponent implements OnInit {

  files: any;
  localisationModal: BsModalRef;
  filter: string;

  id: string;
  doctype: string;
  key: string;

  constructor(
    private data: DataService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private toastr: ToastrService) { }

  ngOnInit(): void {

    this.route.data.subscribe((data: Data) => {
      if (data.doctype) {
        this.doctype = data.doctype;
        return;
      }
      if (data.id) {
        this.id = data.id;
        this.refresh();
      }
    });

    this.route.parent.paramMap.subscribe((paramMap: ParamMap) => {
      this.key = paramMap.get('key');
      if (this.doctype && this.key) {
        this.id = `${this.doctype}:${this.key}`;
        this.refresh();
      }
    });

    this.modalService.onHidden.subscribe(() => {
      this.refresh();
    });
  }

  refresh() {
    this.data.getAssets(this.id, 'files').subscribe(f => { this.files = f; });
  }

  get keys(): string[] {
    return this.files ?
      Object.keys(this.files).sort().filter(v => {
        return this.filter ? v.includes(this.filter) : true;
      }) : [];
  }

  edit(key?: string): void {
    const fileLocalisations = key ? this.files[key] : {};
    this.localisationModal = this.modalService.show(
      LocaleFileModalComponent, {
      initialState: { id: this.id, key, fileLocalisations, folder: 'files' },
      class: 'modal-xl',
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  delete(key: string, prefix?: string, regionCode?: string, languageCode?: string): void {
    if (confirm(`Are you sure you want to delete ${key}`)) {
      this.data.deleteAssetAll(this.id, key, 'files').subscribe(d => {
        this.toastr.success(key, 'Delete file successful');
        this.refresh();
      });
    }
  }
}
