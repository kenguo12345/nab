import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { LocaleService, Locality } from 'src/app/services/locale.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-region-selector',
  templateUrl: './region-selector.component.html'
})
export class RegionSelectorComponent implements OnInit {

  form: FormGroup;
  selectedRegions: string[];
  filter: string;
  @Output() update = new EventEmitter<string[]>();
  controls: {};

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    private locale: LocaleService) { }

  ngOnInit() {
    // Init selectedRegions if it is currently null;
    if (!this.selectedRegions) { this.selectedRegions = []; }
    const controls = {};
    for (const region of this.locale.regions) {
      controls[region.code] = this.selectedRegions.includes(region.code);
    }
    this.form = this.fb.group(controls);
    if (!environment.formsEnabled) this.form.disable();
  }

  get regions(): Locality[] {
    return this.locale.regions.sort().filter(e => {
      return this.filter ? e.name.toLowerCase().includes(this.filter) : [];
    });
  }

  selectAll() {
    Object.keys(this.form.controls).forEach((key) => {
      (<AbstractControl>this.form.controls[key]).setValue(key !== '-');
    });
  }

  selectNone() {
    Object.keys(this.form.controls).forEach((key) => {
      (<AbstractControl>this.form.controls[key]).setValue(false);
    });
  }

  save() {
    const regions = Object.keys(this.form.value).filter(i => this.form.controls[i].value);
    this.update.emit(regions);
    this.bsModalRef.hide();
  }
}
