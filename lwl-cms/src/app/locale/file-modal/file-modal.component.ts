import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import { BsModalRef, TypeaheadMatch } from 'ngx-bootstrap';
import { LocaleService } from 'src/app/services/locale.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/data.service';
import * as _ from 'lodash';

class Localisation {
  region: string;
  regionCode: string;
  language: string;
  languageCode: string;
  value: string;
  public constructor(init?: Partial<Localisation>) { Object.assign(this, init); }
}

@Component({
  templateUrl: './file-modal.component.html',
  styleUrls: ['./file-modal.component.css']
})
export class LocaleFileModalComponent implements OnInit {

  readonly wildcard: string = '-';

  id: string;
  key: string;
  fileLocalisations: any = {};
  folder: string;

  isNew: boolean;
  isKeySet = false;
  form: FormGroup;

  fileInputText = 'Choose file';
  file: File;

  get localisations(): FormArray { return this.form.get('localisations') as FormArray; }

  constructor(
    public bsModalRef: BsModalRef,
    private fb: FormBuilder,
    public locale: LocaleService,
    private data: DataService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.isNew = !this.key;
    this.isKeySet = !this.isNew;
    this.fileLocalisations = this.fileLocalisations || {};
    const defaultValue = _.get(this.fileLocalisations, [this.wildcard, this.wildcard]);
    this.form = this.fb.group({
      key: [this.key, Validators.required],
      value: [defaultValue, Validators.required],
      fileInput: '',
      fileInputText: 'Choose file',
      file: '',
      localisations: this.fb.array([]),
    });
    this.initLocalisations();
  }


  initLocalisations() {
    const regionCodes = Object.keys(this.fileLocalisations);
    for (const regionCode of regionCodes) {
      const regionItem = this.locale.regions.filter(c => c.code === regionCode)[0];
      const languageCodes = Object.keys(this.fileLocalisations[regionCode]);
      for (const languageCode of languageCodes) {
        if (!regionItem || (regionCode === this.wildcard && languageCode === this.wildcard)) { continue; }
        const languageItem = this.locale.languages.filter(c => c.code === languageCode)[0];
        if (!languageItem) continue;
        this.addLocalisation({
          region: regionItem.name,
          regionCode: regionItem.code,
          language: languageItem.name,
          languageCode: languageItem.code,
          value: this.fileLocalisations[regionCode][languageCode],
        });
      }
    }
  }

  setKey() {
    this.isKeySet = true;
  }

  addLocalisation(localisation?: Localisation) {
    const isAddNew = !localisation;
    if (isAddNew) {
      const anyLanguage = this.locale.languages.filter(l => l.code === this.wildcard)[0];
      localisation = new Localisation({
        language: anyLanguage.name,
        languageCode: anyLanguage.code
      });
    }

    (this.localisations as FormArray).push(this.fb.group({
      region: [localisation.region, Validators.compose([Validators.required, regionValidator])],
      regionCode: [localisation.regionCode],
      language: [localisation.language, Validators.compose([Validators.required, languageValidator])],
      languageCode: [localisation.languageCode],
      value: [localisation.value],
      fileInput: '',
      fileInputText: 'Choose file',
      file: '',
    }));
  }

  deleteFile(index: number): void {
    const row = this.localisations.get(`${index}`);
    const noFile = !row.get('value').value;
    if (noFile) { (this.localisations as FormArray).removeAt(index); return; }

    const key = this.form.get('key').value;
    const regionCode = row.get('regionCode').value;
    const languageCode = row.get('languageCode').value;

    if (confirm(`Are you sure you want to delete ${key} [${regionCode}, ${languageCode}]`)) {
      this.data.deleteAsset(this.id, key, 'files', regionCode, languageCode).subscribe(d => {
        (this.localisations as FormArray).removeAt(index);
        this.toastr.success(`${key}<br>Region: ${regionCode}<br>Language: ${languageCode}`, 'Delete file successful');
      });
    }
  }

  onRegionSelect(match: TypeaheadMatch, index: number): void {
    const value = match ? match.item.code : '';
    this.form.get(`localisations.${index}.regionCode`).setValue(value);
    this.form.get(`localisations.${index}.region`).updateValueAndValidity();
  }

  onLanguageSelect(match: TypeaheadMatch, index: number): void {
    const value = match ? match.item.code : '';
    this.form.get(`localisations.${index}.languageCode`).setValue(value);
    this.form.get(`localisations.${index}.language`).updateValueAndValidity();
  }

  // todo: move this somewhere shared
  isInvalid(controlName: string) {
    const control = this.form.get(controlName);
    return control && control.touched && !control.valid;
  }

  fileChange(file: File, index?: number) {
    const prefix = (index !== undefined) ? `localisations.${index}.` : '';
    this.form.get(`${prefix}fileInputText`).setValue(file.name);
    this.form.get(`${prefix}file`).setValue(file);
  }

  uploadFile(e: MouseEvent, index?: number) {
    e.preventDefault();

    const row = index === undefined ? this.form : this.localisations.get(`${index}`);
    const file = row.get('file').value;
    const regionCode = index === undefined ? '' : row.get('regionCode').value;
    const languageCode = index === undefined ? '' : row.get('languageCode').value;
    const key = this.form.get('key').value;

    this.data.uploadAsset(this.id, key, file, this.folder, regionCode, languageCode).subscribe((assetUrl: string) => {
      row.get('file').setValue(undefined);
      row.get('fileInput').setValue('');
      row.get('fileInputText').setValue('Choose file');
      row.get('value').setValue(assetUrl);
      this.toastr.success(`File: ${file.name}<br>Region: ${regionCode || 'default'}
        <br>Language: ${languageCode || 'default'}`, 'Upload file successful');
    });
  }

  undoFile(index?: number) {
    const row = index === undefined ? this.form : this.localisations.get(`${index}`);
    row.get('file').setValue(undefined);
    row.get('fileInput').setValue('');
    row.get('fileInputText').setValue('Choose file');
  }
}

function regionValidator(c: AbstractControl): { [key: string]: boolean } | null {
  const group = c.parent as FormGroup;
  if (!group) { return null; }
  return group.controls.regionCode.value ? null : { hasRegionCode: true };
}

function languageValidator(c: AbstractControl): { [key: string]: boolean } | null {
  const group = c.parent as FormGroup;
  if (!group) { return null; }
  return group.controls.languageCode.value ? null : { hasLanguageCode: true };
}
