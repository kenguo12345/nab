import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './promotion-edit.component.html',
})
export class PromotionEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    {
      name: 'regions', type: 'regions', valueFormatter:
        (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
        defaultValue: ['-']
    },
    { name: 'link', type: 'locale' },
    { name: 'tags', type: 'string', valueFormatter:
      (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: []
    },
    { name: 'labels',
      type: 'objectPicker',
      pickerDocType: 'label',
    valueFormatter:
      (v: string) => v.split(',').filter(f => f).map(m => m.trim()),
      defaultValue: []
    },
    { name: 'order', type: 'locale' },
    { name: 'buttonText', type: 'locale' },
    { name: 'buttonLink', type: 'locale' },
    { name: 'title', type: 'locale' },
    { name: 'copy', type: 'locale', textFieldType: 'textarea' },
    { name: 'subTitle', type: 'locale', textFieldType: 'textarea' },
    { name: 'image.png', type: 'asset' },
    { name: 'modalButtonColour', type: 'string'},
    { name: 'modalButtonOutlineColour', type: 'string'},
    { name: 'modalButtonTextColour', type: 'string'},
    { name: 'modalButtonIcon.png', type: 'asset' },
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
    super(route, fb, data, toastr, modalService);
  }
}
