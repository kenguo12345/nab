import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { BaseFormComponent, FieldDefinition } from 'src/app/shared/base-form.component';

@Component({
  templateUrl: './theme-edit.component.html',
})
export class ThemeEditComponent extends BaseFormComponent {
  fields: FieldDefinition[] = [
    { name: 'logoImage.png', type: 'asset' },
    { name: 'tileImage.png', type: 'asset' },

    { name: 'color', type: 'locale' },
    { name: 'themeColor', type: 'locale' },
    { name: 'buttonColor', type: 'locale' },
    { name: 'lightColor', type: 'locale' },
    { name: 'tileColor', type: 'locale' },
    { name: 'tileAmount', type: 'locale' },
    { name: 'tileVerticalOffset', type: 'locale' },
    { name: 'wineSelectionTabColor', type: 'locale' },
    { name: 'recipeSelectionTabColor', type: 'locale' },
    { name: 'ingredientsBaseColor', type: 'locale' },
    { name: 'ingredientPointColor', type: 'locale' },
    { name: 'directionsBaseColor', type: 'locale' },
    { name: 'tipsAndTricksBaseColor', type: 'locale' },
    { name: 'tipsAndTricksTabColor', type: 'locale' },
    { name: 'textPrimaryColor', type: 'locale' },
    { name: 'textHeadingColor', type: 'locale' }
  ];

  constructor(
    route: ActivatedRoute,
    fb: FormBuilder,
    data: DataService,
    toastr: ToastrService,
    modalService: BsModalService) {
      super(route, fb, data, toastr, modalService);
    }
}
