//to run this file, enter following command in same directory of the file in terminal
//npx ts-node index.ts
const LUNCH = "Lunch";
const BREAKFAST = "Breakfast";
const BRUNCH = "Brunch";

for(var i=50;i<=500;i++){
    let output="";

    if(i%3===0) { output += BREAKFAST;}
    if(i%5===0) { output += LUNCH;}

    if(output=== (BREAKFAST+LUNCH)){ output= BRUNCH;}
    if(output==="") { output=String(i);}

    console.log(output);
}

